﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using OpenCvSharp;
using System.Drawing;
using System.IO;

namespace Hac
{
    class Read
    {
        string url;
        BitmapImage bmps;
        bool start = false;
        public Timer timer1;
        public MainWindow mmw;
        public Read(string url, MainWindow mw)
        {
            mmw = mw;
            bmps = null;
            this.url = url;
        }
        ~Read()
        {
            try
            {
                start = false;
            }
            finally
            {

            }
        }
        public void Start()
        {
            if (timer1 != null)
                timer1.Dispose();

            var file = VideoCapture.FromFile(url);
            var origin = new Mat();
            //int sleepTime = (int)Math.Round(1000 / file.Fps);
            int sleepTime = 1;
            Mat image = new Mat();
            Mat result;
            double minVal, maxVal, threshold = 0.5;
            OpenCvSharp.Point minLoc, maxLoc;
            Mat template = new Mat("3.png");
            Mat template1 = new Mat("2.png");
            //Mat r2 = new Mat();


            while (file.Read(origin) == true)
            //while (true)

            {
                file.Read(origin);
                if (start == false)
                    break;

                result = new Mat(origin.Rows - template.Rows + 1, origin.Cols - template.Cols + 1, MatType.CV_32FC1);

                Cv2.MatchTemplate(origin, template, result, TemplateMatchModes.CCoeffNormed);
                Cv2.Threshold(result, result, 0.0, 1.0, ThresholdTypes.Tozero);
                Cv2.MinMaxLoc(result, out minVal, out maxVal, out minLoc, out maxLoc);
                if (maxVal >= threshold)
                {
                    Rect r = new Rect(new OpenCvSharp.Point(maxLoc.X, maxLoc.Y), new OpenCvSharp.Size(template.Width, template.Height));
                    Cv2.Rectangle(origin, r, Scalar.Red, 2);
                    Rect outRect;
                    Cv2.FloodFill(result, maxLoc, new Scalar(0), out outRect, new Scalar(0.1), new Scalar(1.0), FloodFillFlags.Link4);
                }
                Console.WriteLine("Ковш вероятность: " + maxVal);


                Cv2.MatchTemplate(origin, template1, result, TemplateMatchModes.CCoeffNormed);
                Cv2.Threshold(result, result, 0.0, 1.0, ThresholdTypes.Tozero);
                Cv2.MinMaxLoc(result, out minVal, out maxVal, out minLoc, out maxLoc);
                if (maxVal >= threshold)
                {
                    Rect r = new Rect(new OpenCvSharp.Point(maxLoc.X, maxLoc.Y), new OpenCvSharp.Size(template1.Width, template1.Height));
                    Cv2.Rectangle(origin, r, Scalar.LimeGreen, 2);
                    Rect outRect;
                    Cv2.FloodFill(result, maxLoc, new Scalar(0), out outRect, new Scalar(0.1), new Scalar(1.0), FloodFillFlags.Link4);
                }

                //Console.WriteLine("Ковш вероятность: " + maxVal);
                //System.Windows.Application.Current.Dispatcher.BeginInvoke(new Action(() => mmw.textBlock.Text += "Ковш вероятность: " + maxVal + "\n"));


                Cv2.MatchTemplate(origin, template1, result, TemplateMatchModes.CCoeffNormed);
                Cv2.Threshold(result, result, 0.0, 1.0, ThresholdTypes.Tozero);
                Cv2.MinMaxLoc(result, out minVal, out maxVal, out minLoc, out maxLoc);
                if (maxVal >= threshold)
                {
                    Rect r = new Rect(new OpenCvSharp.Point(maxLoc.X, maxLoc.Y), new OpenCvSharp.Size(template1.Width, template1.Height));
                    Cv2.Rectangle(origin, r, Scalar.LimeGreen, 2);
                    Rect outRect;
                    Cv2.FloodFill(result, maxLoc, new Scalar(0), out outRect, new Scalar(0.1), new Scalar(1.0), FloodFillFlags.Link4);
                }
                Console.WriteLine("Вероятность котла " + maxVal);
                //Cv2.ImShow("Результат", origin);
                int character = Cv2.WaitKey((int)Math.Round(file.Fps));
                //Console.WriteLine("Вероятность котла " + maxVal);
                //System.Windows.Application.Current.Dispatcher.BeginInvoke(new Action(() => mmw.textB.Text += "Вероятность котла " + maxVal + "\n"));

                // Cv2.ImShow("Результат", origin);
                //int character = Cv2.WaitKey((int)Math.Round(file.Fps));


                bmps = BitmapToImageSource(OpenCvSharp.Extensions.BitmapConverter.ToBitmap(origin));
                bmps.Freeze();
                Thread.Sleep(sleepTime);
            }



        }
        public BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();
                return bitmapimage;
            }
        }
        public void Draw(MainWindow mw)
        {

            if (bmps != null)
            {
                System.Windows.Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    try
                    {
                        mw.myImage.Source = bmps;

                    }
                    catch { }
                }));

            }
        }
        public bool Starting()
        {
            return start = true;
        }
        public bool Stop()
        {
            return start = false;
        }
    }

}


