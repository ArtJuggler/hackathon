﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.OleDb;
using System.Data;
using MaterialDesignThemes.Wpf;

namespace bd_kurs_work_2018
{
    /// <summary>
    /// Interaction logic for BD.xaml
    /// </summary>
    public partial class BD : Window
    {
        OleDbConnection bd = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\smirn\Desktop\BD\bd_kurs_work_2018\MainDB2.mdb");
        OleDbCommand command;
        OleDbDataReader read;

        string s;

        public BD()
        {
            InitializeComponent();
        }

        private void DataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            bd.Open();
            DataSet vDs = new DataSet();
            OleDbDataAdapter dbAdapter10 = new OleDbDataAdapter("SELECT * FROM Actions", bd);
            dbAdapter10.Fill(vDs);
            this.dg.ItemsSource = vDs.Tables[0].DefaultView;
            dg.ItemsSource = vDs.Tables[0].DefaultView;
            bd.Close();
     }

        private void button_Click(object sender, RoutedEventArgs e)
        {

            string addr = "qweweqweq";
            string command_add = "INSERT INTO `Actions` (`Type`, `Time`) VALUES ('" + addr + "','" + DateTime.Now.ToString() + "')";
            OleDbCommand command1 = new OleDbCommand(command_add, bd);
    
            bd.Open();
         
            command1.ExecuteNonQuery();
            bd.Close();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            bd.Open();
            DataSet vDs = new DataSet();
            OleDbDataAdapter dbAdapter10 = new OleDbDataAdapter("SELECT * FROM Actions", bd);
            dbAdapter10.Fill(vDs);
            this.dg.ItemsSource = vDs.Tables[0].DefaultView;
            dg.ItemsSource = vDs.Tables[0].DefaultView;
            bd.Close();
        }
    }
}
