﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using OpenCvSharp;
using OpenCvSharp.Blob;
using OpenCvSharp.ML;
using System.ComponentModel;

namespace Hac
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : System.Windows.Window
    {
        string file = "";
        bool DBcheck;
        BitmapImage frame;
        Thread thread;
        Read p;
        public MainWindow()
        {
            InitializeComponent();
            CompositionTarget.Rendering += Draw;
        }
        public void Draw(object sender, EventArgs e)
        {
            if (p != null)
                p.Draw(this);
        }

        /* private void Analozation(String path) {
             using (Mat template = new Mat("3.png"), template1 = new Mat("2.png"))
             {
                 VideoCapture move = new VideoCapture(path);
                 Mat image = new Mat();
                 Mat result, result1;
                 double minVal, maxVal, threshold = 0.5;
                 Point minLoc, maxLoc;
                 while (move.Read(image))
                 {
                     result = new Mat(image.Rows - template.Rows + 1, image.Cols - template.Cols + 1, MatType.CV_32FC1);
                     result1 = new Mat(image.Rows - template1.Rows + 1, image.Cols - template1.Cols + 1, MatType.CV_32FC1);
                     Cv2.MatchTemplate(image, template, result, TemplateMatchModes.CCoeffNormed);
                     Cv2.Threshold(result, result, 0.0, 1.0, ThresholdTypes.Tozero);
                     Cv2.MinMaxLoc(result, out minVal, out maxVal, out minLoc, out maxLoc);
                     if (maxVal >= threshold)
                     {
                         Rect r = new Rect(new Point(maxLoc.X, maxLoc.Y), new Size(template.Width, template.Height));
                         Cv2.Rectangle(image, r, Scalar.Red, 2);
                         Rect outRect;
                         Cv2.FloodFill(result, maxLoc, new Scalar(0), out outRect, new Scalar(0.1), new Scalar(1.0), FloodFillFlags.Link4);
                     }
                     Console.WriteLine("Ковш вероятность: " + maxVal);


                     Cv2.MatchTemplate(image, template1, result, TemplateMatchModes.CCoeffNormed);
                     Cv2.Threshold(result, result, 0.0, 1.0, ThresholdTypes.Tozero);
                     Cv2.MinMaxLoc(result, out minVal, out maxVal, out minLoc, out maxLoc);
                     if (maxVal >= threshold)
                     {
                         Rect r = new Rect(new Point(maxLoc.X, maxLoc.Y), new Size(template1.Width, template1.Height));
                         Cv2.Rectangle(image, r, Scalar.LimeGreen, 2);
                         Rect outRect;
                         Cv2.FloodFill(result, maxLoc, new Scalar(0), out outRect, new Scalar(0.1), new Scalar(1.0), FloodFillFlags.Link4);
                     }
                     Console.WriteLine("Вероятность котла " + maxVal);
                     Cv2.ImShow("Результат", image);
                     int character = Cv2.WaitKey((int)Math.Round(move.Fps));
                 }
             }
         }*/

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            try
            {
                this.thread.Abort();
            }
            catch
            { this.Close(); }
        }

        private void SelectVideo_Click(object sender, RoutedEventArgs e) //Загрузка видео
        {
            Microsoft.Win32.OpenFileDialog openFileDialog1 = new Microsoft.Win32.OpenFileDialog();
            openFileDialog1.Filter = "(*.*)|*.*";
            openFileDialog1.ShowDialog();
            if (openFileDialog1.FileName.Length > 0)
            {
                file = openFileDialog1.FileName;
            }

            if (file != "")
            {
                var frame = OpenCvSharp.VideoCapture.FromFile(file);
                var r = new OpenCvSharp.Mat();
                frame.Read(r);
                BitmapImage b = BitmapToImageSource(OpenCvSharp.Extensions.BitmapConverter.ToBitmap(r));
                myImage.Source = b;
                System.Windows.Application.Current.Dispatcher.BeginInvoke(new Action(() => myImage.Source = b));
            }

        }

        //private BitmapImage BitmapToImageSource(Bitmap bitmap)
        //{
        //    throw new NotImplementedException();
        //}

        public BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();
                return bitmapimage;
            }
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            if (thread == null)
            {
                if (file == "") return;
                p = new Read(file, this);
                p.Starting();
                thread = new Thread(new ThreadStart(p.Start));
                thread.Start();

                Start.Content = "Стоп";
                //myImageBox.BorderBrush = System.Windows.Media.Brushes.Black;
            }
            else
            {
                thread.Abort();
                p.Stop();
                Start.Content = "Старт";
                thread = null;
            }

            
        }

        private void DBWriteCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            DBcheck = true;
        }

        private void DBWriteCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            DBcheck = false;
        }

        //class img
        //{
        //    OpenCvSharp.Mat image;

        //    private void BitMapToBitMapImage()
        //    {
        //        // Tried this way but nothing is shown
        //        image = new OpenCvSharp.Mat;
        //        MainWindow.Video.image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(image);


        //    }
        //}
    }
}
